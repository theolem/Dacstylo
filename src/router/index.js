import Vue from 'vue'
import VueRouter from 'vue-router'
import Screen from '../components/Screen.vue'
import StatScreen from '../components/StatScreen.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Screen
  },
  {
    path: '/stats',
    name: 'stats',
    component: StatScreen,
    props: true
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
